#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()
{
	//Creación del fichero, el puntero a la memoria y el puntero para la proyección
	int file;
	DatosMemCompartida* pMemComp;
	char *proyeccion;

	//Apertura y proyección del fichero en memoria
	file=open("/tmp/datosBot.txt",O_RDWR);
	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
	close(file);

	//Apuntamos el puntero de datos a la proyección del fichero en memoria.
	pMemComp=(DatosMemCompartida*)proyeccion;

	//Control raqueta
	bool salir=true;
	
	while(salir){
		if(pMemComp->accion==5){
			salir=false;
		}
		usleep(25000);
		float posRaqueta;
		posRaqueta=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		if(posRaqueta<pMemComp->esfera.centro.y)
			pMemComp->accion=1;
		else if(posRaqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;
		else
			pMemComp->accion=0;

	}
	//Desmontamos la proyección de memoria.
	munmap(proyeccion,sizeof(*(pMemComp)));
}
