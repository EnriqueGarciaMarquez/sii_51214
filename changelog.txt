 Se ha cambiado el nombre del directorio de practica4 a practica5
- Se han modificado las cabeceras de los archivos Esfera.h, Mundo.h, Plano.h, Raqueta.h, Vector2D.h

PRACTICA 2

- Se ha añadido la funcionalidad de disminuir el tamaño de la pelota a medida que avanza el juego en Esfera.h
- Se ha añadido la funcionalidad de mover la raqueta del jugador 1 pulsando las teclas S y W.

PRACTICA 3

- Se ha implementado el logger para que el programa saque por consola los puntos de los jugadores, aunque entra en un bucle infinito al finalizar el juego y se queda bloqueada la consola.
- Se ha implementado el bot para que la máquina controle la raqueta 1 pero no funciona bien. 
- Solucionado el problema del bot y añadida la funcionalidad de control automático de la raqueta 2 tras 10 segundos sin pulsar ninguna tecla.

PRACTICA 4

- Se han sustituido los archivos tenis.cpp y Mundo.cpp (y su .h) por MundoCliente.cpp, MundoServidor.cpp, Cliente.cpp y Servidor.cpp
- Modificado el CMakeLists.txt de la carpeta practica1/src para crear los programas cliente y servidor.
- Se ha implementado el envío de coordenadas del servidor al cliente mediante las tuberias pipecs y pipekb, que comunican cliente y servidor.
- Se ha implementado el envío de las teclas de cliente a servidor mediante threads.
